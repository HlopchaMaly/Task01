package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a;
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println("0 - выход\nВведи число:");
            a = scanner.nextInt();
            if((a%3)==0){
                System.out.println(a+" делится на 3. Результат: "+a/3);
            }
            else {
                System.out.println(a+" не делится нацело на 3");
            }
        }while (a!=0);
    }
}
